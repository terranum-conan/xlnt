# XLNT

## Build XLNT package locally

1. download the github receipe from here : https://github.com/shajeen/conan-xlnt

          wget https://github.com/shajeen/conan-xlnt/archive/refs/tags/v1.5.0.tar.gz
          tar -xf v1.5.0.tar.gz
  
2. build locally
   
          cd conan-xlnt-1.5.0
          conan create . terranum-conan+xlnt/stable

   For Windows, add the following option : `-o xlnt:shared=True` because non shared version of xlnt didn't work and leads to compilation errors at link time.
   
3. debug version 

      The xlnt "receipe" didn't fully support debug version. 
      To enable debug version replace the last lines of conanfile.py with the following:

           def package_info(self):
               self.cpp_info.libs = tools.collect_libs(self)

      and then create the package using : `conan create . terranum-conan+xlnt/stable -s build_type=Debug`.

4. Upload the package to GitLab

        conan upload xlnt/1.5.0@terranum-conan+xlnt/stable --remote=gitlab --all

## Use XLNT package

1. Install the Gitlab remote

        conan remote add gitlab https://gitlab.com/api/v4/packages/conan

2. Add a credentiel for all GitLab repositories linked to the terranum-conan group

    1. Generate a personnal token or an access token. See help here : https://docs.gitlab.com/ee/user/packages/conan_repository/

    2. Add the credential : 
    
            conan user <gitlab_username or deploy_token_username> -r gitlab -p <personal_access_token or deploy_token>`

3. Search the repository

        conan search 'xlnt*' --remote=gitlab

4. Consume the package

    In the 'conanfile.txt' add the following 

        [requires]
        xlnt/1.5.0@terranum-conan+xlnt/stable
        ...

